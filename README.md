	# gulpfile.js

## Instructions

- `git clone https://sundamo@bitbucket.org/sundamo/gulpfile.js.git` - Clone "gulpfile.js" anywhere below your theme directory. I have gulpfile.js cloned into the same web root directory as all of my projects so that they can all access a single instance of the gulp files. You could also clone the gulpfile.js directory into your theme directory or your wp-contents directory, or the your project root directory, etc...
- `cp gulpfile.js\resources\package.json .` - Copy gulpfile.js\resources\package.json to the same directory as this gulpfile.js directory
- `npm i` or `'yarn` - Install package.json dependencies.
- `cp gulpfile.js\resources\child-theme\config.json wp-content/themes/YOUR_THEME` - Copy resources\child-theme\config.json to the your theme root directory and modify as needed.
- `cd wp-content/themes/YOUR_THEME` - go to the theme directory to call the gulp commands. 
	- `gulp js` - compile js source files into js
	- `gulp css` - compile scss files into css
	- `gulp vars` - create files defined in vars attributes in config.json file:
	  - `src/scss/_variables-colors.scss`
		- `src/scss/_variables-colors-blocks.css`
		- `inc/editor-color-palette.php`

	- `gulp` - compile everything and watch files for changes
  - `gulp --tasks` - list all gulp tasks 

For more details open gulpfile.js/index.js and see the bottom of the file for all exported gulp commands

## Nalu

"Nalu Gulp" functionality is now merged into "gulpfile.js".
Update Degree Finder breakpoint in config file,

```
"config": {
	"breakpoints": { "lg": "992px" },
```

- "lg" value can be found in bootstrap \_variable.scss,
  `../nalu/node_modules/bootstrap/scss/_variables.scss` or `themes\nalu\src\scss\_variables.scss`, etc..
- `gulp init` is now accompanied by `gulp initDF` which replaces the breakpoint in the degree finder plugin.

## Upgrade

Old

```
"config": {
	"dest": {
		"js": "./assets/js/site.js",
```

New

```
"src": {
	"js": {
		"three-test.js": [
			"src/js/utils.js",
			"src/js/site-three.js"
		],
		"site.js":       [
			"src/js/*.js"
		]
	},
},
"dest": {
	"js": "./assets/js/",
}
```
