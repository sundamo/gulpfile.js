const fs = require('fs')
//const path        = require('path');

//let parentDir  = process.cwd() + '/'; // parent directory
let callingDir = process.env.INIT_CWD // calling directory - parent or child theme dir
/**
 * @param config.browserSync - https://www.browsersync.io/docs/options
 * @param config.cleanCss
 * @param config.src
 * @param config.dest
 * @param config.watch
 */

/**
 * @type {{
 * 		browserSync: {},
 * 		breakpoints: {
 * 			xs:string, sm:string, md:string, lg:string, xl:string 
 * 		},
 * 		vars: {colors:{}},
 * 		cleanCss: {},
 * 		babel: {},
 * 		uglify: {},
 * 		copyFiles: [],
 * 		watch: {
 * 			scss:string
 * 			js:string
 * 			config:string
 * 		},
 * 		src: {
 * 			scss:string,
 * 			css:string,
 * 			js:{},
 * 			fonts:string,
 * 			img:string,
 * 		},
 * 		dest: {
 * 		  	css:string,
 * 			js:string,
 * 			fonts:string,
 * 			img:string,
 * 		}
 * }} gulp config
 */
let config = getConfig()

// change working directory to the calling directory.
// This effects all src() and dest(). This does not effect require paths.
process.chdir(callingDir)

/**
 * Look fot config in calling dir as
 * 1. config.json
 * 2. package.json
 * @returns {*}
 */
function getConfig() {
	console.log('Source directory ' + callingDir)

	let config
	if (fs.existsSync(callingDir + '/gulpconfig.json')) {
		config = require(callingDir + '/gulpconfig.json').config
	} else if (fs.existsSync(callingDir + '/config.json')) {
		config = require(callingDir + '/config.json').config
		// } else if (fs.existsSync(callingDir + '/package.json')) {
		// 	config = require(callingDir + '/package.json').config
	}
	console.log(config)
	if (!config) {
		throw new Error('No config.json file found! Make sure you are in the correct directory');
	}
	return config
}

exports.callingDir = callingDir
exports.config     = config
exports.getConfig  = getConfig

//function setConfig() {
// merge child and parent config - maybe not useful after all...
//config          = require(process.cwd() + '/gulpconfig.json'); // parent
//let childConfig = process.env.INIT_CWD + '/gulpconfig.json'; // child
//if (fs.existsSync(childConfig)) {
//	const merge = require('deepmerge');
//	config      = merge(config, require(childConfig), {
//		arrayMerge: (destinationArray, sourceArray) => sourceArray,
//	});
//}
//}
