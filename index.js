const { parallel, series, task } = require('gulp')
const gulp_watch = require('gulp-watch')
const browserSync = require('browser-sync').create()
const { fonts } = require('./fonts')
const { copyFiles } = require('./copy-files')
const { createVars } = require('./create-vars')
const { img } = require('./images')
const { compile, css, js } = require('./compile')
const { initDF } = require('./compile-nalu')
const { incVersion } = require('./inc-version')
let { config, getConfig } = require('./config')

function bSync() {
	return browserSync.init(config.browserSync)
}

function syncReload(e) {
	console.log("reload");
	
	browserSync.reload()
	return e
}

function watch() {

	if (config.watch.css) gulp_watch(config.watch.css, series(css))
	if (config.watch.pcss) gulp_watch(config.watch.pcss, series(css))
	if (config.watch.scss) gulp_watch(config.watch.scss, series(css))

	if (config.watch.js) gulp_watch(config.watch.js, series(js))
	if (config.watch.config) gulp_watch(config.watch.config, series(getConfig)) //, parallel(compileJS, compileCSS))); // need to

	if (config.watch.reload) gulp_watch(config.watch.reload, syncReload)

	return Promise.resolve() // re-spawn gulp
}

// gulp
exports.test = function (cb) {
	cb()
}
exports.initDF = initDF
exports.init = parallel(createVars, copyFiles, fonts, img, series(compile)) // called npm postinstall
exports.vars = createVars
exports.copy = copyFiles
exports.fonts = fonts
exports.img = img
exports.inc = parallel(incVersion)
exports.css = css
exports.js = js
exports.compile = compile // css and js
exports.default = exports.watch = exports.dev = series(compile, watch, bSync)

