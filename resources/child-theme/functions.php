<?php
if (!defined('WP_DEBUG')) {
	die('Direct access forbidden.');
}

define('ABSPATH_CHILD', __DIR__);
define('ABSPATH_CHILD_URI', get_stylesheet_directory_uri());
define('ABSPATH_PARENT_URI', get_template_directory_uri());
define('CHILD_THEME_VERSION', wp_get_theme()->get('Version'));
define('DISABLE_NAG_NOTICES', true); // remove divi popup admin notices

// require all inc/ files
foreach (glob(__DIR__ . "/inc/{,*/,*/*/,*/*/*/}*.php", GLOB_BRACE) as $file) {
	require $file;
}