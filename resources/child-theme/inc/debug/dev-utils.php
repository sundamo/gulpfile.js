<?php

// Trigger gulp watch php files
if (WP_DEBUG) {
	add_action(
		'wp_update_nav_menu',
		function () {
			touch(get_template_directory() . '\temp.php');
			touch(get_stylesheet_directory() . '\temp.php');
		}
	);
	add_action('save_post', function () {
		touch(get_template_directory() . '\temp.php');
		touch(get_stylesheet_directory() . '\temp.php');
	});
}


if (!function_exists('d') && !function_exists('dd')) {

	/**
	 * Example
	 * global $_wp_additional_image_sizes;
	 * dd( $_wp_additional_image_sizes );
	 *
	 * @param mixed $obj
	 * @param bool  $print
	 *
	 * @return  mixed
	 */
	function d($obj = '', $print = true)
	{
		$obj = '<pre style="position: absolute; z-index: 9999999999999; background:#000;color:#fff; padding:40px;">' .
			preg_replace('(\d+\s=>)', "", var_export($obj, true)) .
			'</pre>';
		if ($print) echo $obj;

		return $obj;
	}

	/**
	 * @param mixed $obj
	 */
	function dd($obj = '')
	{
		die(d($obj, true));
	}
}
