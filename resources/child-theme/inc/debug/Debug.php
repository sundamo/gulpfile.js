<?php

namespace Sun\Utils;

class Debug {

	private static $instance;
	public         $debug_obj;

	/**
	 * Optional Global instance
	 *
	 * @return \Sun\Utils\Debug
	 */
	public static function instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Debug constructor.
	 *
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * @param $obj - add item to the debug log
	 */
	public function log( $obj ) {
		//$this->check();
		$this->debug_obj[] = $obj;
	}

	private function init() {
		$this->debug_obj = [];
		add_action( 'wp_footer', function() {
			if ( $this->debug_obj ) {
				foreach ( $this->debug_obj as $obj ) {
					echo '<script>console.log(' . json_encode( $obj ) . ')</script>';
				}
			}
		}, 99999 );
	}
}
