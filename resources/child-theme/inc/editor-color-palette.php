<?php
add_theme_support( 'editor-color-palette', [
// Defined in package.json. Created by Gulp: `gulps vars`
	[ 'name' => 'primary', 'slug' => 'primary', 'color' => '#003435', ],
	[ 'name' => 'secondary', 'slug' => 'secondary', 'color' => '#00b53a', ],
	[ 'name' => 'peace', 'slug' => 'peace', 'color' => '#c5f7c4', ],
	[ 'name' => 'energy', 'slug' => 'energy', 'color' => '#0fbcbc', ],
	[ 'name' => 'passion', 'slug' => 'passion', 'color' => '#b2f9f7', ],
	[ 'name' => 'dark', 'slug' => 'dark', 'color' => '#000', ],
	[ 'name' => 'light', 'slug' => 'light', 'color' => '#eee', ],
	[ 'name' => 'grey', 'slug' => 'grey', 'color' => '#777', ],
] );
