<?php

/**
 * SITE CSS & JS
 */
add_action('wp_enqueue_scripts', function () {
	$deps = ['jquery'];
	// remove jquery
	if (!is_admin() && !is_customize_preview()) {
		wp_deregister_script('jquery');
		$deps = null;
	}
	// remove child style.css
	wp_dequeue_style('generate-child');
	// parent
	wp_enqueue_style('site-parent-css', ABSPATH_PARENT_URI . '/style.css', null, CHILD_THEME_VERSION);
	// child
	wp_enqueue_style('site-child-css', ABSPATH_CHILD_URI . '/assets/css/site.css', null, CHILD_THEME_VERSION);
	wp_enqueue_script('site-child-js', ABSPATH_CHILD_URI . '/assets/js/site.js', $deps, CHILD_THEME_VERSION, true);
}, 300);

/**
 * BLOCK EDITOR
 * Enqueue supplemental block editor js and styles.
 */
add_action('enqueue_block_editor_assets', function () {
	/**
	 * .is-style-floating {
	 *  background-color: #0087be;
	 * }
	 */
	wp_enqueue_style('site_admin_blocks_css', ABSPATH_CHILD_URI . '/assets/css/site-admin-blocks.css', ['wp-edit-blocks'], CHILD_THEME_VERSION);
	/**
	 * wp.blocks.registerBlockStyle('core/image', {
	 *  name:  'floating',
	 *  label: 'Floating',
	 * });
	 */
	wp_enqueue_script('site_admin_blocks_js', ABSPATH_CHILD_URI . '/assets/js/site-admin-blocks.js', ['jquery'], CHILD_THEME_VERSION, true);
}, 1, 1);

/**
 * WP ADMIN LOGIN
 */
add_action('login_head', function () {
	wp_enqueue_style('site_admin_login_css', ABSPATH_CHILD_URI . '/assets/css/site-admin-login.css', false, CHILD_THEME_VERSION);
	//wp_enqueue_script( 'site_admin_login_js', ABSPATH_CHILD_URI . '/assets/js/site-admin-login.js', [ 'jquery' ], CHILD_THEME_VERSION, true );
});

/**
 *  WP ADMIN
 */
//add_action( 'admin_head', function() {
//	wp_enqueue_style( 'site_admin_css', ABSPATH_CHILD_URI . '/assets/css/site-admin.css', false, CHILD_THEME_VERSION );
//	wp_enqueue_script( 'site_admin_js', ABSPATH_CHILD_URI . '/assets/js/site-admin.js', [ 'jquery' ], CHILD_THEME_VERSION, true );
//} );