const { src, dest, series, parallel } = require('gulp')

const fs = require('fs')
const { incVersion } = require('./inc-version')
const uglify = require('gulp-uglify-es').default
const concat = require('gulp-concat')
const replace = require('gulp-replace')
const plumber = require('gulp-plumber')
const path = require('path')
const rename = require('gulp-rename');

// const browserify = require('browserify')
// var reactify = require('reactify');
// var source = require('vinyl-source-stream');
// const named = require('vinyl-named');
const named = require('vinyl-named-with-path');
var through = require('through')

const webpack = require('webpack-stream');
const compiler = require('webpack');

const babel = require('gulp-babel')
const gulpif = require('gulp-if')
var debug = require('gulp-debug')
// var gulpif = require('gulp-if-else');

const postcss = require('gulp-postcss');
const postcss_scss = require('postcss-scss');
const postcssPlugins = [
	require('postcss-import'),
	require('postcss-nested'),
	require('postcss-short-color'),
	require('postcss-preset-env'),
	require('postcss-custom-media'),
	require('cssnano')({ preset: 'default' }),
	// require('postcss-strip-inline-comments'),
	// require('postcss-warn-cleaner'),
	// require('postcss-short'),
	// require('postcss-cssnext'),
	// require('postcss-custom-properties')({
	// 	"no-value-notifications": false,
	// 	noValueNotifications: false,
	// 	warnings: {
	// 		"noValueNotifications": false,
	// 		"no-value-notifications": false,
	// 		"not-scoped-to-root": false,
	// 		"circular-reference": false
	// 	}
	// }),
]
//// const clean_css    = require('gulp-clean-css')

let { config } = require('./config')


// COMPILE

// CSS

function compileCSS() {
	// console.log("config", config);

	let items = config.src?.css ?? config.src?.scss // config - old to new (css, scss both work)
	if (items?.constructor !== Array) items = [items]

	for (let item of items) {
		if (item.constructor === String) item = { src: item } // config - old to new
		if (!item.src) continue;

		if (!item.dest) item.dest = config.dest?.css // config - old to new 
		if (!item.dest) continue;

		let p = path.parse(item.dest)
		if (p.ext) item.dest = p.dir // dest must be a directory

		let cleanCss = config.cleanCss ?? {
			level: {
				1: {
					all: true,
					specialComments: 0,
				},
				2: {
					restructureRules: true,
				},
			},
		}

		src(item.src, { allowEmpty: true, sourcemaps: (item?.sourcemaps == true || config.mode == 'development') })
			.pipe(plumber())
			//.pipe(concat(p.base))
			.pipe(postcss(postcssPlugins,
				{ parser: postcss_scss }
				// {syntax: postcss_scss}
			))
			//.pipe(autoprefixer()) // browser list is not moved to package.json  ..."browserslist": [ "defaults", "> 1%", "last 2 versions", "ie >= 11" ]
			// .pipe(clean_css(cleanCss))
			.pipe(rename({
				extname: (item.postfix ?? '') + ".css"
			}))
			.pipe(dest(item.dest, { sourcemaps: '.' }))
	}
	return Promise.resolve()
}


function compileJS() {
	let items = config.src?.js
	if (items?.constructor !== Array) items = [items]

	for (let item of items) {
		// let name = path.parse(item).name
		if (item.constructor === String) item = { src: item } // config - old to new
		if (!item.src) continue;
		console.log("---item.src", item.src);

		if (!item.dest) item.dest = config.dest?.js // config - old to new 
		if (!item.dest) continue;
		console.log("---item.dest", item.dest);

		// if (item.src === String) item.src = [item.src] // allow adding multiple js into one file
		// for (let src of item.src) {

		// let srcObj = path.parse(item.src)
		let destObj = path.parse(item.dest)
		// console.log("---destObj", destObj);
		
		const pipeWebpack = webpack({
			mode: config.mode ?? 'production',
			devtool: item?.sourcemaps ? 'cheap-source-map' : false // does not work with uglify
		}, compiler)

		// export single file from 1+ js files.
		// {
		// 	"src": [ "./src/js/site.js", "./src/js/src/top-table-control.js" ],
		// 	"dest": "./assets/js/site.js"
		// },
		if (destObj.ext) {
			src(item.src, { allowEmpty: true, sourcemaps: item?.sourcemaps ?? true })
				.pipe(plumber())
				.pipe(named())
				.pipe(pipeWebpack)
				.pipe(debug({ title: '-----' }))
				.pipe(concat(destObj.base))
				.pipe(dest(destObj.dir), { sourcemaps: '.' })
		}

		// export multiple files from multiple files
		// {
		// 	"src": "./src/components/*/*.js",
		// 	"dest": "./assets/components/"
		// }
		else {
			src(item.src, { allowEmpty: true, sourcemaps: item?.sourcemaps ?? true })
				.pipe(plumber())
				.pipe(named())
				.pipe(pipeWebpack)
				// .on('data', (data) => { console.log(data) })
				.pipe(debug({ title: '-----' }))
				.pipe(dest(destObj.dir + '/' + destObj.base), { sourcemaps: '.' })
		}
	}
	return Promise.resolve()
}

exports.css = parallel(incVersion, compileCSS)
exports.js = parallel(incVersion, compileJS)
exports.compile = parallel(incVersion, compileCSS, compileJS)
