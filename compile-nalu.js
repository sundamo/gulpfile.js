const { src, dest } = require('gulp')
const replace = require('gulp-replace')
const fs = require('fs')

let { config } = require('./config')

// replace degree-finder breakpoints based on config.breakpoints.lg
async function replaceDFBreakpoint() {
  if (config.breakpoints && config.breakpoints.lg)
    fs.stat(
      '../../plugins/degree-finder/class-degree_finder.php',
      async (err) => {
        if (err == null) {
          // await getBSBreakpoint()
          return src('../../plugins/degree-finder/class-degree_finder.php')
            .pipe(
              replace(
                /BREAK_POINT\s*?=\s*?['"]?(.)+['"]?;/g,
                'BREAK_POINT = ' + config.breakpoints.lg + ';',
              ),
            )
            .pipe(dest('../../plugins/degree-finder/'))
            .on('end', () => {
              console.log('replaced degree-finder breakpoints')
            })
        } else if (err.code === 'ENOENT')
          console.log('degree-finder plugin does not exist: ', err.code)
        else console.log('error: ', err.code)
      },
    )
  else {
    console.log(
      'compileDF replaceDFBreakpoint failed. config.breakpoints.lg does not exist',
    )
  }
  return Promise.resolve()
}

// get bootstrap breakpoints from:
// ..\nalu\node_modules\bootstrap\scss\_variables.scss

// function getBSBreakpoint() {
//   if (breakpoint) return Promise.resolve()
//   return new Promise((resolve) => {
//     fs.readFile(
//       '../nalu/node_modules/bootstrap/scss/_variables.scss',
//       'utf8',
//       async (err, data) => {
//         if (err) console.log('Error: compile.js > getBSBreakpoint()', err)
//         else {
//           let regex = await /(?:\$grid-breakpoints[\s\S]+?lg:\s*?)([\S]+?)(?:px+?)/g
//           breakpoint = (await regex.exec(data)[1]) || 0
//           console.log('found bootstrap breakpoint: ' + breakpoint)
//         }
//         resolve()
//       },
//     )
//   })
// }

exports.initDF = replaceDFBreakpoint
// exports.compileDF = series(getBSBreakpoint, replaceDFBreakpoint)
