const fs = require('fs');

//let config = require('./config').config;

function incVersion() {
	let file = './style.css'
	if (fs.existsSync(file)) {
		let text  = fs.readFileSync(file, 'utf8');
		let lines = text.split(/\r?\n/);
		lines.forEach(function(item, i, arr) {
			if (item.indexOf('Version') !== -1) {
				let nums = (item.replace(/[^\d\.]*/g, '')).split('.');
				nums[nums.length - 1]++;
				arr[i] = 'Version: ' + nums.join('.');
			}
		});
		text = lines.join('\n');
		if (text) {
			// console.log(text);
			fs.writeFileSync(file, text, 
				// function(err, result) {
				// 	if (err) console.log('error', err);
				// 	if (result) console.log('result', result);
				// }
			);
		} else {
			console.log('gulp incVersion failed');
		}
	}

	return Promise.resolve();
}

exports.incVersion = incVersion
