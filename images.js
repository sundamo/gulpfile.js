const {src, dest} = require('gulp')
const plumber     = require('gulp-plumber')
// const imagemin    = require('gulp-imagemin') // Minify PNG, JPEG, GIF and SVG images with imagemin.

let config = require('./config').config

// images
//https://www.npmjs.com/package/gulp-imagemin
// module.exports = function imagemin() {
// 	return import('gulp-imagemin').then((gulpImagemin) => {
function img() {

	return import('gulp-imagemin').then((imagemin) => {

		console.log(config.src.img + ' -> ' + config.dest.img)

		if (!config.src.img || !config.dest.img) return Promise.resolve()

		let production = true
		if (production) {
			return src(config.src.img)
				.pipe(plumber())
				.pipe(
					imagemin.default(
						// [
						// 	imagemin.gifsicle({interlaced: true}),
						// 	imagemin.mozjpeg({progressive: true}),
						// 	imagemin.optipng({optimizationLevel: 7}),
						// 	imagemin.svgo({
						// 		plugins: [{removeViewBox: false}, {cleanupIDs: false}],
						// 	}),
						// ],
						{
							verbose: true,
						},
					),
				)
				.pipe(dest(config.dest.img))
		} else {
			return src(config.src.img).pipe(plumber()).pipe(dest(config.dest.img))
		}
	})
}

exports.img = img
