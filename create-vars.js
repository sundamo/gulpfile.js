const fs     = require('fs');
let {config} = require('./config');

// todo: we could do custom font sizes here as well

function createVars() {
	if (config.vars && config.vars.colors) {
		let comment = "Created via `gulp vars`. Defined in theme's config (config.json, gulpconfig.json or package.json)"
		let dataScss  = ''
		let dataScss2 = ''
		let dataScss3 = '\n'
		let dataPhp   = ''

		console.log("Create:")
		console.log("  _variables-colors.scss")
		console.log("  editor-color-palette.php")

		for (let i in config.vars.colors) {
			let color = config.vars.colors[i].replace('$', '').replace(';', '').split(':')
			let key   = color[0]
			let val   = color[1]

			console.log(`    🎨 ${key} = ${val}`);

			dataScss += `$${key}: ${val};\n`;
			dataScss2 += `.has-${key}-background-color {background-color: ${val};}\n`;
			dataScss3 += `.has-${key}-color {color: ${val};}\n`;
			dataPhp += `\t[ 'name' => '${key}', 'slug' => '${key}', 'color' => '${val}', ],\n`;
		}

		dataScss2 += dataScss3;
		fs.writeFileSync('src/scss/_variables-colors.scss', `// ${comment}\n${dataScss}`);
		fs.writeFileSync('src/scss/_variables-colors-blocks.css',`/* ${comment} */\n${dataScss2}` );
		fs.writeFileSync('inc/editor-color-palette.php', `<?php\n// ${comment}\nadd_theme_support( 'editor-color-palette', [\n${dataPhp}] );\n`);
	}

	return Promise.resolve();
}

exports.createVars = createVars;
