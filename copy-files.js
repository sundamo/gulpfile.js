const { src, dest } = require('gulp')
const fs = require('fs')
const path = require('path')
const concat = require('gulp-concat')
let config = require('./config').config

// called from package.json postinstall

/**
 * src: string, globs, arrays
 * dest: dir, or file.
 *    If file then src will be concat into file name.
 *    If path then the src files will be copied
 * @returns {Promise<void>}
 */
function copyFiles() {
  if (!config.copyFiles) return Promise.resolve()

  let pattern = /^((http|https|ftp):\/\/)/

  let files = Array.isArray(config.copyFiles)
    ? config.copyFiles
    : [config.copyFiles]

  for (let i in files) {
    if (!files.hasOwnProperty(i)) continue

    let srcFile = files[i].src
    let destFile = files[i].dest
    if (!srcFile || !destFile) continue

    console.log('copy src: ' + srcFile)
    let p = path.parse(destFile)
    // URL
    if (pattern.test(srcFile)) {
      let requirePath = srcFile.indexOf('https') == 0 ? 'https' : 'http'
      let http = require(requirePath)
      console.log(`    dest: ${p.dir}`)
      let file = fs.createWriteStream(destFile)
      let request = http
        .get(srcFile, function (response) {
          response.pipe(file)
          file.on('finish', function () {
            file.close() // close() is async, call cb after close completes.
          })
        })
        .on('error', (err) => {
          console.log(`    ERROR: ${err}`)
          return fs.unlink(destFile, null)
        })
    }
    // File
    else {
      console.log(`    dest: ${destFile}`)
      // concat source file(s) into dest file
      if (!fs.existsSync(srcFile)) {
        console.log(`    ERROR: Does not exist!`)
        continue
      }
      if (p.ext) {
        src(srcFile).pipe(concat(p.base)).pipe(dest(p.dir))
      }
      // copy source file(s) to new directory
      else {
        src(srcFile).pipe(dest(destFile))
      }
    }
  }
  return Promise.resolve()
}

exports.copyFiles = copyFiles
