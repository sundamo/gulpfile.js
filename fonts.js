const { src, dest, series, parallel } = require('gulp')
// const GetGoogleFonts = require('get-google-fonts');
let config = require('./config').config;

//https://www.npmjs.com/package/gulp-google-webfonts
let googleWebFonts = require('gulp-google-webfonts');

/**
 * use, get-google-fonts
 * https://www.npmjs.com/package/get-google-fonts
 * Consider downloading fonts manually via https://google-webfonts-helper.herokuapp.com/
 */
function processFonts() {

	if (!config.src.fonts || !config.dest.fonts) return Promise.resolve();

	// return new GetGoogleFonts().download(config.src.fonts, {
	// 	outputDir:   config.dest.fonts,//  './assets/fonts/',
	// 	overwriting: true,
	// 	verbose:     true,
	// 	path:        './',
	// 	base64:      false, //  true to embed the font in the font.css file
	// 	templateX:   '{_family}-{weight}-{comment}-{style}.{ext}',
	// 	//}).then(() => {
	// }).catch((e) => {
	// 	console.log(e)
	// })

	return src('./fonts.txt')
		.pipe(googleWebFonts({
			// fontsDir: '',
			// cssDir: '',
			// cssFilename: 'myGoogleFonts.css',
			format:'woff2'
		}))
		.pipe(dest('assets/fonts'))
}

exports.fonts = processFonts;


// old nalu font system

//const gfonts         = require('gulp-gfonts');
//function writeGfontFile() {
//	return new Promise(async resolve => {
//		let gfontsObj = {};
//		for (let font of config.src.fonts) {
//
//			console.log(font);
//			console.log('-----')
//
//			let italic = await font.style === 'italic' ? 'i' : '';
//
//			gfontsObj[font.family] = await (typeof (gfontsObj[font.family]) === 'undefined')
//															 ? font.weight + italic
//															 : gfontsObj[font.family] + ',' + font.weight + italic;
//		}
//		console.log(gfontsObj)
//		console.log('-----')
//
//		//fs.writeFile('./fonts.json', JSON.stringify(gfontsObj), () => {
//		//	resolve();
//		//	console.log('PUT FONTS.JSON');
//		//});
//
//		return new Promise(resolve => {
//			src(gfontsObj, {base: '.'})
//					.pipe(gfonts({formats: ['woff2'], inCssBase: '../fonts'}))
//					.pipe(dest('.'))
//					.on('end', () => {
//						console.log('GOT FONTS');
//						resolve();
//					});
//		});
//	});
//}
//
//function getGoogleFonts() {
//	return new Promise(resolve => {
//		gulp.src('./fonts.json')
//				.pipe(gfonts({
//											 formats:   ['woff2'],
//											 inCssBase: '../fonts',
//										 }))
//				.pipe(gulp.dest(nalu))
//				.on('end', () => {
//					console.log('GOT FONTS');
//					resolve();
//				});
//	});
//
//}

//function writeFontCSS() {
//	return new Promise(resolve => {
//		let fontStr = '';
//		for (let font of fonts) {
//			console.log(font.family + " -> " + font.weight + " -> " + font.mixin);
//
//			let newFontStr = `
//@mixin ` + font.mixin + `() {
//	font-family: '` + font.family + `', $font-family-sans-serif;
//	font-weight: ` + font.weight + `;
//	font-style: ` + font.style + `;
//	text-rendering: optimizeLegibility;
//};`;
//
//			fontStr = fontStr.concat(newFontStr);
//		}
//
//
//		fs.writeFile('src/scss/_fonts.scss', fontStr, () => {
//			resolve()
//		});
//	});
//}

